# DEPRECATED, JUST BUILD YOUR OWN™

# marionette-popup-region
Implements the nice [Magnific Popup](https://github.com/dimsemenov/Magnific-Popup) - made by [dimsemenov](https://github.com/dimsemenov) - in a `Marionette.Region`.

Magnific Popup is a dependency of this package. As such, do not include it in your require.js config or package.json.

## Install

Install with npm. Then add this to your require.js config file:
```js
'shim': {
    'marionette-popup-region': {
        'deps': ['jquery', 'marionette', 'underscore']
    }
}
```

And add the popup to a region:

```js
var RPopup = require('marionette-popup-region');

app.addRegions({
    popup: RPopup
});
```

By default the region uses the `#_popup` element, so add a `<div id="_popup"></div>` to your page.

In your SCSS, add the following line:
```scss
@import '../../app/node_modules/marionette-popup-region/marionette-popup-region';
```

To edit colors and some basic style settings, simply copy/paste this [_settings.scss file](https://github.com/dimsemenov/Magnific-Popup/blob/master/src/css/_settings.scss).

## Usage

| __Option__  | __Description__ | __Type__  | __Default__ |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|-------------|
| `type`      | The type of popup: `inline`, `image`, or `iframe`.                                                                                                                                                                                                          | `string`  | `inline`    |
| `src`       | If set, this will be rendered instead of the view.                                                                                                                                                                                                          | `mixed`   | `false`       |
| `focus`     | Focus on this element when the popup opens.                                                                                                                                                                                                                 | `string`  | `''`        |
| `modal`     | When set to true, the popup will have a modal-like behavior: it won’t be possible to dismiss it by usual means (close button, escape key, or clicking in the overlay). [Read more](http://dimsemenov.com/plugins/magnific-popup/documentation.html#modal). | `boolean` | `false`     |
| `animation` | Enable a 300ms fade animation.                                                                                                                                                                                                                              | `boolean` | `true`      |

Display a view in the popup:
```js
var VTest = require('view/test');
app.popup.show(new VTest);
```

Add some options to it:

```js
app.popup.setParams({
    animation: false,
    modal: true
});

var VTest = require('view/test');
app.popup.show(new VTest);
```

To display a photo or video:

```js
app.popup.setParams({
    type: 'image',
    src: 'nice-photo.jpg'
});

app.popup.showPopup();
```

```js
app.popup.setParams({
    type: 'iframe',
    src: 'http://vimeo.com/2619976'
});

app.popup.showPopup();
```

When inserting a view, use this template:
```html
<div class="popup-block">
    <button title="Close (Esc)" class="mfp-close">×</button>
</div>
```

To close the popup from a button / whatever, use the `._destroy` class.

[Documentation of Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/documentation.html)

## Todo
- Allow multiple `src`, so image galleries are possible.
- Allow all settings of Magnific Popup (at the moment only a subset is possible).