define(function (require) {
    'use strict';

    var Marionette = require('marionette'),
        $ = require('jquery'),
        _ = require('underscore');

    return Marionette.Region.extend({
        el: '#_popup',
        // Default parameters for Magnific Popup.
        // These can be overriden by setParams().
        params: {
            type: 'inline', // e.g. 'inline', 'iframe' or 'image'
            src: false, // Can be a URL if type is 'iframe' or 'image'
            focus: '', // Element in popup to automatically focus on
            modal: false, // Set to true to disable easy closing
            animation: true,
        },
        setParams: function (customParams) {
            _.merge(this.params, customParams);
        },
        constructor: function (options) {
            Marionette.Region.prototype.constructor.call(this, options);

            this.on('show', this.showPopup, this);
        },
        showPopup: function (view) {
            if (view) {
                view.on('destroy', this.hidePopup, this);

                view.$el.on('click', '._destroy', function () {
                    this.hidePopup();
                }.bind(this));
            }

            $.magnificPopup.open({
                items: {
                    src: this.params.src || this.$el,
                    focus: this.params.focus,
                },
                type: this.params.type,
                modal: this.params.modal,
                // Don't show close buttons when type is inline,
                // because we will manually insert them.
                showCloseBtn: this.params.type !== 'inline',
                closeBtnInside: this.params.type !== 'inline',
                removalDelay: this.params.animation ? 300 : 0,
                mainClass: this.params.animation ? 'mfp-fade' : '',
                fixedContentPos: false,
                callbacks: {
                    close: function () {
                        this.hidePopup();
                    }.bind(this)
                }
            });
        },
        hidePopup: function () {
            // Hide popup
            $.magnificPopup.close();

            // Empty out the region
            if (this.currentView) {
                this.currentView.destroy();
            }
        }
    });
});
