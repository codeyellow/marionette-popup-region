'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass');

var jsFiles = [
        'node_modules/magnific-popup/src/js/core.js',
        'node_modules/magnific-popup/src/js/iframe.js',
        'node_modules/magnific-popup/src/js/image.js',
        'node_modules/magnific-popup/src/js/inline.js',
        'node_modules/magnific-popup/src/js/retina.js',
        'src/marionette-popup-region.js'
    ],
    distJsFile = 'marionette-popup-region.js',
    distJsMinFile = 'marionette-popup-region.min.js',
    cssFiles = ['src/marionette-popup-region.scss'],
    distDir = 'dist';

gulp.task('build-js', function () {
    return gulp.src(jsFiles)
        .pipe(concat(distJsFile))
        .pipe(gulp.dest(distDir))
        .pipe(uglify())
        .pipe(rename(distJsMinFile))
        .pipe(gulp.dest(distDir));
});

gulp.task('build-css', function () {
    return gulp.src(cssFiles)
        .pipe(sass())
        .pipe(gulp.dest(distDir));
});

/*
 * Default task.
 */
gulp.task('build', ['build-js', 'build-css']);
